package controllers

import (
	"fmt"
	"net/http"
	"github.com/gin-gonic/gin"
	"x/models"
)

func InsertOrder(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")
  
	var order models.Orders
	c.BindJSON(&order)
	
	addOrder := models.Orders{Customer_Name: order.Customer_Name, Ordered_At: order.Ordered_At}
	if err := models.DB.Create(&addOrder).Error; err != nil {
	  fmt.Printf("error create Order: %3v \n", err)
	  c.AbortWithStatus(http.StatusInternalServerError)
	  return
	}

	for _, item := range order.Items {
		if err := models.DB.Create(&models.Items{Order_Id: addOrder.Order_Id, Item_Code: item.Item_Code, Description: item.Description, Quantity: item.Quantity}).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
	}
  
	c.JSON(http.StatusOK, gin.H{"data": "success"})
}


func GetOrders(c *gin.Context) {
	orders := []models.Orders{}
	models.DB.Find(&orders)

	dtOrders := []models.Orders{}
	for _, order := range orders {
		items := []models.Items{}
		models.DB.Where("order_id = ?", order.Order_Id).Find(&items)
		order.Items = items
		dtOrders = append(dtOrders, order)
	}

	c.JSON(http.StatusOK, dtOrders)
}

func UpdateOrder(c *gin.Context) {
	var order models.Orders
	if err := c.BindJSON(&order); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	orderDt := models.Orders{}

	if err := models.DB.Where("order_id = ?", c.Param("orderid")).First(&orderDt).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Order not found!"})
		return
	}

	if err := models.DB.Model(&orderDt).Updates(&models.Orders{Customer_Name: order.Customer_Name, Ordered_At: order.Ordered_At}).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	for _, item := range order.Items {
		oldItem := models.Items{}

		if err := models.DB.Where("order_id = ? AND item_id = ?", c.Param("orderid"), item.Item_Id).First(&oldItem).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Data not found!"})
			return
		}

		if err := models.DB.Model(&oldItem).Updates(&models.Items{Order_Id: oldItem.Order_Id, Item_Id: oldItem.Item_Id, Item_Code: item.Item_Code, Description: item.Description, Quantity: item.Quantity}).Error; err != nil {
			panic(err)
		}
	}

	c.JSON(http.StatusOK, gin.H{"data": "success"})
}

func DeleteOrder(c *gin.Context) {
	order := models.Orders{}
	items := []models.Items{}

	if err := models.DB.Where("order_id = ?", c.Param("orderid")).First(&items).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Data not found!"})
		return
	}

	if err := models.DB.Where("order_id = ?", c.Param("orderid")).First(&order).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Order not found!"})
		return
	}

	fmt.Println(&items)

	models.DB.Delete(models.Items{}, "order_id = ?", c.Param("orderid"))
	models.DB.Delete(&order)

	c.JSON(http.StatusOK, gin.H{"data": "success"})
}