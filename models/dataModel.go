package models

import (
	"time"
)

type Orders struct{
	Order_Id  int `json:"order_id" gorm:"primaryKey;autoIncrement"`
	Customer_Name  string `json:"customer_name"`
	Ordered_At time.Time `json:"ordered_at"`
	Items []Items  `json:"items" gorm:"foreignKey:Order_Id;references:Order_Id;"`
}

type Items struct{
	Item_Id  int `json:"item_id" gorm:"primaryKey;autoIncrement"`
	Item_Code  string `json:"item_code"`
	Description  string `json:"description"`
	Quantity  int `json:"quantity"`
	Order_Id  int `json:"order_id"`
}

