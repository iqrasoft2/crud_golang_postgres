package main

import (
  "time"
  "fmt"
  "os"
  "github.com/gin-gonic/gin"
  "x/controllers"
  "x/models"

)

func main() {
  os.Setenv("TZ", "Asia/Jakarta")
  fmt.Printf("Started at : %3v \n", time.Now())

  models.ConnectDatabase()

  gin.SetMode(gin.ReleaseMode)
  router := gin.Default()

  api := router.Group("/api")

  api.POST("/orders", controllers.InsertOrder)
  api.GET("/orders", controllers.GetOrders)
  api.PUT("/orders/:orderid", controllers.UpdateOrder)
  api.DELETE("/orders/:orderid", controllers.DeleteOrder)

  router.Run(":4000")
}